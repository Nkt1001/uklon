package uklonapp.appossum.awesome.uklonapp.api

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import uklonapp.appossum.awesome.uklonapp.Injection
import uklonapp.appossum.awesome.uklonapp.TestUtil
import uklonapp.appossum.awesome.uklonapp.persistence.FakeDb
import java.util.concurrent.TimeUnit

/**
 * Created by nikitaemelyanov on 04.05.2018.
 */
@RunWith(JUnit4::class)
class ApiRepositoryTest {

    lateinit var dataSource: ApiRepository
    lateinit var dataSourceMock: ApiRepository
    lateinit var apiService: ApiService
    val compositeDisposable = CompositeDisposable()

    @Before fun init() {
        apiService = mock(ApiService::class.java)
        dataSourceMock = ApiRepository(apiService, FakeDb)

        dataSource = Injection.provideDataSource()
    }

    @After fun dispose() {
        compositeDisposable.clear()
    }

    @Test fun insertOrUpdateCommentsTest() {

        var comments = TestUtil.createComments(10)
        dataSource.insertOrUpdateComment(comments)
        compositeDisposable.add(dataSource.fetchCommentsFromDb().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.size == 10})

        dataSource.insertOrUpdateComment(comments)
        compositeDisposable.add(dataSource.fetchCommentsFromDb().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.size == 10})

        comments = TestUtil.createComments(25)
        dataSource.insertOrUpdateComment(comments)
        compositeDisposable.add(dataSource.fetchCommentsFromDb().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.size == 35})


        val moreComments = ArrayList(comments)
        moreComments.addAll(TestUtil.createComments(10))
        dataSource.insertOrUpdateComment(moreComments)
        compositeDisposable.add(dataSource.fetchCommentsFromDb().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.size == 45})
    }

    @Test fun insertOrUpdatePostsTest() {
        var posts = TestUtil.createPosts(10)
        dataSource.insertOrUpdatePosts(posts)
        compositeDisposable.add(dataSource.fetchPostsFromDb().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.size == 10})

        dataSource.insertOrUpdatePosts(posts)
        compositeDisposable.add(dataSource.fetchPostsFromDb().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.size == 10})

        posts = TestUtil.createPosts(25)
        dataSource.insertOrUpdatePosts(posts)
        compositeDisposable.add(dataSource.fetchPostsFromDb().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.size == 35})


        val morePosts = ArrayList(posts)
        morePosts.addAll(TestUtil.createPosts(10))
        dataSource.insertOrUpdatePosts(morePosts)
        compositeDisposable.add(dataSource.fetchPostsFromDb().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.size == 45})
    }

    @Test fun badResponsePostTest() {

        dataSourceMock.insertOrUpdatePosts(TestUtil.createPosts(10))

        `when`(apiService.getPosts()).thenReturn(Observable.create({
            it.onError(Exception("some error"))
        }))

        compositeDisposable.add(dataSourceMock.fetchPosts().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.isNotEmpty()})
    }

    @Test fun badResponseCommentTest() {
        dataSourceMock.insertOrUpdateComment(TestUtil.createComments(10))

        `when`(apiService.getComments()).thenReturn(Observable.create({
            it.onError(Exception("some error"))
        }))

        compositeDisposable.add(dataSourceMock.fetchComments().test().awaitDone(1, TimeUnit.SECONDS).assertValue{it.isNotEmpty()})
    }
}