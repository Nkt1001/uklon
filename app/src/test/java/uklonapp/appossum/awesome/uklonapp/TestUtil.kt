package uklonapp.appossum.awesome.uklonapp

import uklonapp.appossum.awesome.uklonapp.persistence.Comment
import uklonapp.appossum.awesome.uklonapp.persistence.Post
import java.util.*

object TestUtil {

    val random = Random()

    fun createPost() = Post(random.nextInt(), random.nextInt(), UUID.randomUUID().toString(), UUID.randomUUID().toString() + System.currentTimeMillis())

    fun createPosts(count: Int): List<Post> {
        return (0 until count).map {
            createPost()
        }
    }

    fun createComment() = Comment(random.nextInt(), random.nextInt(), "hello@world", UUID.randomUUID().toString(),
            UUID.randomUUID().toString() + System.currentTimeMillis())

    fun createComments(count: Int): List<Comment> {
        return (0 until count).map {
            createComment()
        }
    }

}