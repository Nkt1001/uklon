package uklonapp.appossum.awesome.uklonapp.ui

import android.arch.lifecycle.ViewModel
import io.reactivex.Completable
import io.reactivex.Observable
import uklonapp.appossum.awesome.uklonapp.api.ApiRepository
import uklonapp.appossum.awesome.uklonapp.persistence.Post

/**
 * Created by nikitaemelyanov on 03.05.2018.
 */
class PostViewModel(private val dataSource: ApiRepository): ViewModel() {
    fun updateDatabase(posts: List<Post>): Completable {
        return Completable.fromAction { dataSource.insertOrUpdatePosts(posts) }
    }

    fun getPosts(): Observable<List<Post>> {
        return dataSource.fetchPosts()
    }

}