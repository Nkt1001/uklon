package uklonapp.appossum.awesome.uklonapp.ui

import uklonapp.appossum.awesome.uklonapp.R
import uklonapp.appossum.awesome.uklonapp.persistence.Post


/**
 * Created by nikitaemelyanov on 04.05.2018.
 */
class NavigationController constructor(mainActivity: MainActivity) {
    private val containerId = R.id.container
    private val fragmentManager = mainActivity.supportFragmentManager

    fun navigateToPosts() {
        val postFragment = PostFragment()
        fragmentManager.beginTransaction()
                .replace(containerId, postFragment)
                .commitAllowingStateLoss()
    }

    fun navigateToComments(post: Post) {
        val fragment = CommentFragment.create(post)
        val tag = "comment/${post.id}/${post.userId}"
        fragmentManager.beginTransaction()
                .replace(containerId, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }
}