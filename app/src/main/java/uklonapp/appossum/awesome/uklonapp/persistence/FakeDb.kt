package uklonapp.appossum.awesome.uklonapp.persistence

object FakeDb {

    val postTable = ArrayList<Post>()

    val commentTable = ArrayList<Comment>()

    fun insertPosts(posts: List<Post>) = postTable.addAll(posts)

    fun insertComments(comments: List<Comment>) = commentTable.addAll(comments)

    fun getCommentsById(id: Int): List<Comment> = commentTable.filter { it.id == id }
}