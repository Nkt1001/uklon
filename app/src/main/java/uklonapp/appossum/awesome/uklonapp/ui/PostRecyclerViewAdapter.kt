package uklonapp.appossum.awesome.uklonapp.ui

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_post.view.*
import uklonapp.appossum.awesome.uklonapp.R
import uklonapp.appossum.awesome.uklonapp.persistence.Post

class PostRecyclerViewAdapter(val listener: OnPostClickedListener) : ListAdapter<Post, PostRecyclerViewAdapter.ViewHolder>(object : DiffUtil.ItemCallback<Post>() {
    override fun areItemsTheSame(oldItem: Post?, newItem: Post?): Boolean = (oldItem == newItem)
    override fun areContentsTheSame(oldItem: Post?, newItem: Post?): Boolean  = (oldItem == newItem) }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_post, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    inner class ViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {

        private val context = mView.context

        fun bind(item: Post) {
            mView.setOnClickListener { listener.onClicked(item) }

            mView.post_id.text = context.getString(R.string.post_id, item.id.toString())
            mView.post_userId.text = context.getString(R.string.user_id, item.userId.toString())
            mView.post_title.text = item.title
            mView.post_body.text = item.body
        }
    }
}
