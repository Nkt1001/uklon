package uklonapp.appossum.awesome.uklonapp

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import uklonapp.appossum.awesome.uklonapp.api.ApiRepository
import uklonapp.appossum.awesome.uklonapp.api.ApiService
import uklonapp.appossum.awesome.uklonapp.persistence.FakeDb
import uklonapp.appossum.awesome.uklonapp.ui.ViewModelFactory

object Injection {

    fun provideApiService(): ApiService {
        return Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiService::class.java)
    }

    fun provideDataSource(): ApiRepository {
        return ApiRepository(provideApiService(), FakeDb)
    }

    fun provideViewModelFactory(): ViewModelFactory {
        val dataSource = provideDataSource()
        return ViewModelFactory(dataSource)
    }
}
