package uklonapp.appossum.awesome.uklonapp.api

import io.reactivex.Observable
import retrofit2.http.GET
import uklonapp.appossum.awesome.uklonapp.persistence.Comment
import uklonapp.appossum.awesome.uklonapp.persistence.Post

interface ApiService {
    @GET("/posts")
    fun getPosts(): Observable<List<Post>>

    @GET("/comments")
    fun getComments(): Observable<List<Comment>>
}