package uklonapp.appossum.awesome.uklonapp.ui

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_comment.view.*
import uklonapp.appossum.awesome.uklonapp.R
import uklonapp.appossum.awesome.uklonapp.persistence.Comment


class CommentRecyclerViewAdapter
    : ListAdapter<Comment, CommentRecyclerViewAdapter.ViewHolder>(object : DiffUtil.ItemCallback<Comment>() {
            override fun areItemsTheSame(oldItem: Comment?, newItem: Comment?): Boolean = (oldItem?.id == newItem?.id
                    && oldItem?.postId == newItem?.postId)
            override fun areContentsTheSame(oldItem: Comment?, newItem: Comment?): Boolean = (oldItem?.body == newItem?.body)
        }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_comment, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    inner class ViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {

        private val context = mView.context

        fun bind(item: Comment) {


            mView.comment_id.text = context.getString(R.string.id, item.id.toString())
            mView.comment_postId.text = context.getString(R.string.post_id, item.postId.toString())
            mView.comment_name.text = context.getString(R.string.name, item.name)
            mView.comment_email.text = context.getString(R.string.email, item.email)
            mView.comment_body.text = item.body
        }
    }
}
