package uklonapp.appossum.awesome.uklonapp.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_comment_list.*
import kotlinx.android.synthetic.main.fragment_post.*
import uklonapp.appossum.awesome.uklonapp.Injection
import uklonapp.appossum.awesome.uklonapp.R
import uklonapp.appossum.awesome.uklonapp.persistence.Comment
import uklonapp.appossum.awesome.uklonapp.persistence.Post

class CommentFragment : Fragment() {

    private lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: CommentViewModel
    private val disposable = CompositeDisposable()

    private var mPost: Post? = null

    private lateinit var commentAdapter: CommentRecyclerViewAdapter

    companion object {
        private const val ARG_POST = "uklonapp.appossum.awesome.uklonapp.args.ARG_POST"

        fun create(post: Post): CommentFragment {

            val b = Bundle()
            b.putParcelable(ARG_POST, post)

            val fragment = CommentFragment()
            fragment.arguments = b

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPost = arguments?.getParcelable(ARG_POST)

        viewModelFactory = Injection.provideViewModelFactory()
        viewModel = viewModelFactory.create(CommentViewModel::class.java)
        commentAdapter = CommentRecyclerViewAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_comment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        post_id.text = getString(R.string.post_id, mPost?.id.toString())
        post_userId.text = getString(R.string.user_id, mPost?.userId.toString())
        post_title.text = mPost?.title
        post_body.text = mPost?.body

        list.layoutManager = LinearLayoutManager(context)
        list.adapter = commentAdapter

        activity?.content_layout?.setOnRefreshListener { refreshData() }

    }

    private fun refreshData() {

        activity?.content_layout?.isRefreshing = true

        disposable.add(viewModel.getComments()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    activity?.content_layout?.isRefreshing = false
                    if (it.isNotEmpty()) {
                        disposable.add(viewModel.updateDatabase(it)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe())

                        showComments(it.filter { it.postId == mPost?.id })
                    } else {
                        showEmpty()
                    }
                }, {
                    activity?.content_layout?.isRefreshing = false
                    showEmpty()
                }))
    }

    override fun onStart() {
        super.onStart()
        refreshData()
    }

    override fun onStop() {
        super.onStop()

        disposable.clear()
    }

    private fun showEmpty() {
        Toast.makeText(context, "Network error", Toast.LENGTH_LONG).show()
    }

    private fun showComments(commentList: List<Comment>) {
        commentAdapter.submitList(commentList)
    }
}
