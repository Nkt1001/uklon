package uklonapp.appossum.awesome.uklonapp.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import uklonapp.appossum.awesome.uklonapp.R

class MainActivity : AppCompatActivity() {

    private lateinit var navigationController: NavigationController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        navigationController = NavigationController(this)

        if (savedInstanceState == null) {
            navigationController.navigateToPosts()
        }

    }
}
