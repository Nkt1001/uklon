package uklonapp.appossum.awesome.uklonapp.ui

import android.arch.lifecycle.ViewModel
import io.reactivex.Completable
import io.reactivex.Observable
import uklonapp.appossum.awesome.uklonapp.api.ApiRepository
import uklonapp.appossum.awesome.uklonapp.persistence.Comment

class CommentViewModel(private val dataSource: ApiRepository): ViewModel() {

    fun updateDatabase(comments: List<Comment>): Completable {
        return Completable.fromAction { dataSource.insertOrUpdateComment(comments) }
    }

    fun getComments(): Observable<List<Comment>> {
        return dataSource.fetchComments()
    }
}