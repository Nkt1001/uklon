package uklonapp.appossum.awesome.uklonapp.ui

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import uklonapp.appossum.awesome.uklonapp.api.ApiRepository

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val dataSource: ApiRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PostViewModel::class.java)) {
            return PostViewModel(dataSource) as T
        } else if (modelClass.isAssignableFrom(CommentViewModel::class.java)) {
            return CommentViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
