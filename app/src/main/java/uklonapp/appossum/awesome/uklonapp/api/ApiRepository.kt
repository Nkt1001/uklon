package uklonapp.appossum.awesome.uklonapp.api

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import uklonapp.appossum.awesome.uklonapp.persistence.Comment
import uklonapp.appossum.awesome.uklonapp.persistence.FakeDb
import uklonapp.appossum.awesome.uklonapp.persistence.Post

class ApiRepository(private val apiService: ApiService, private val db: FakeDb) {

    companion object {
        private const val RETRY_COUNT: Long = 3
    }

    fun fetchPosts(): Observable<List<Post>> {
        return apiService.getPosts()
                .retry(RETRY_COUNT)
                .onErrorResumeNext(fetchPostsFromDb()
                        .subscribeOn(Schedulers.io()))
    }

    fun fetchComments(): Observable<List<Comment>> {
        return apiService.getComments()
                .retry(RETRY_COUNT)
                .onErrorResumeNext(fetchCommentsFromDb()
                        .subscribeOn(Schedulers.io()))
    }

    fun fetchPostsFromDb(): Observable<List<Post>> {
        return Observable.create {

            if (db.postTable.isNotEmpty()) {
                it.onNext(db.postTable)
                it.onComplete()
            } else {
                it.tryOnError(Exception("No values!"))
            }
        }
    }

    fun fetchCommentsFromDb(): Observable<List<Comment>> {

        return Observable.create {

            if (db.commentTable.isNotEmpty()) {
                it.onNext(db.commentTable)
                it.onComplete()
            } else {
                it.tryOnError(Exception("No values!!"))
            }
        }

    }

    fun insertOrUpdatePosts(postList: List<Post>) {
        val savedPosts = db.postTable
        val uPosts =  postList.filterNot { savedPosts.contains(it) }
        db.insertPosts(uPosts)
    }

    fun insertOrUpdateComment(commentList: List<Comment>) {
        val savedComments = db.commentTable
        val uComments =  commentList.filterNot { savedComments.contains(it) }
        db.insertComments(uComments)
    }
}