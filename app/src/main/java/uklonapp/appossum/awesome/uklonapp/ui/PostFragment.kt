package uklonapp.appossum.awesome.uklonapp.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_post_list.*
import uklonapp.appossum.awesome.uklonapp.Injection
import uklonapp.appossum.awesome.uklonapp.R
import uklonapp.appossum.awesome.uklonapp.persistence.Post

class PostFragment : Fragment() {

    private lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: PostViewModel

    private val disposable = CompositeDisposable()
    private lateinit var adapter: PostRecyclerViewAdapter
    private lateinit var navigationController: NavigationController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModelFactory = Injection.provideViewModelFactory()
        viewModel = viewModelFactory.create(PostViewModel::class.java)

        navigationController = NavigationController(activity as MainActivity)

        adapter = PostRecyclerViewAdapter(object : OnPostClickedListener {
            override fun onClicked(post: Post) {
                navigationController.navigateToComments(post)
            }
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return layoutInflater.inflate(R.layout.fragment_post_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.content_layout?.setOnRefreshListener { refreshData() }

        list.layoutManager = LinearLayoutManager(context)
        list.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        refreshData()
    }

    private fun refreshData() {
        activity?.content_layout?.isRefreshing = true

        disposable.add(viewModel.getPosts()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    activity?.content_layout?.isRefreshing = false
                    if (it.isNotEmpty()) {
                        showPosts(it)
                    } else {
                        showEmpty()
                    }
                }, {
                    activity?.content_layout?.isRefreshing = false
                    showEmpty()
                }))
    }

    override fun onStop() {
        super.onStop()

        disposable.clear()
    }

    private fun showPosts(postList: List<Post>) {
        disposable.add(viewModel.updateDatabase(postList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe())

        adapter.submitList(postList)
    }

    private fun showEmpty() {
        Toast.makeText(context, "Network error", Toast.LENGTH_LONG).show()
    }
}
