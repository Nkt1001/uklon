package uklonapp.appossum.awesome.uklonapp.persistence

import com.google.gson.annotations.SerializedName

/**
 * Created by nikitaemelyanov on 03.05.2018.
 */
data class Comment(@SerializedName("postId") val postId: Int,
                   @SerializedName("id") val id: Int,
                   @SerializedName("email") val email: String,
                   @SerializedName("name") val name: String,
                   @SerializedName("body") val body: String)