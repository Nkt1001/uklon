package uklonapp.appossum.awesome.uklonapp.ui

import uklonapp.appossum.awesome.uklonapp.persistence.Post

interface OnPostClickedListener {
    fun onClicked(post: Post)
}